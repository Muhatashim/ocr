package org.virus.ocr.core.scanner;

import java.awt.*;
import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: Muhatashim
 * Date: 12/31/13
 * Time: 1:44 PM
 */
public class SmartObject<T extends SmartType & Serializable> implements Serializable {

    private T         t;
    private Rectangle area;

    /**
     * @param t    the object located at <code>area</code>
     * @param area the area which contains <code>t</code>
     */
    public SmartObject(T t, Rectangle area) {
        this.t = t;
        this.area = area;
    }

    public T get() {
        return t;
    }

    public Rectangle getArea() {
        return area;
    }
}
