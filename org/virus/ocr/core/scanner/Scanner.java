package org.virus.ocr.core.scanner;

import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: Muhatashim
 * Date: 12/31/13
 * Time: 1:42 PM
 */
public interface Scanner {

    /**
     * Scans the image for detected smart objects
     *
     *
     * @param image
     * @return
     */
    java.util.List<SmartObject<SmartType>> scan(Image image);
}
