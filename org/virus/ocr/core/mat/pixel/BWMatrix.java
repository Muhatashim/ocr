package org.virus.ocr.core.mat.pixel;

import org.virus.ocr.core.mat.Matrix;

/**
 * Created with IntelliJ IDEA.
 * User: Muhatashim
 * Date: 12/31/13
 * Time: 2:00 PM
 */
public class BWMatrix extends Matrix implements OCR<BWPixelMatrix> {

    /**
     * Creates an array with the specified initial array.
     * Precondition: map.length > 0
     *
     * @see org.virus.ocr.core.mat.pixel.BWPixelMatrix#generateBWMap(java.awt.image.BufferedImage);
     */
    public BWMatrix(float[][] map) {
        super(map);
    }

    @Override
    public float match(BWPixelMatrix mat) {
        checkDimensions(mat.getMatrix());

        float[][] myMatrix = getMatrix();
        float totalCorrect = 0;
        float total = 0;

        for (int x = 0; x < getWidth(); x++) {
            for (int y = 0; y < getHeight(); y++) {
                float curr = myMatrix[x][y];

                total += curr;

                if (mat.isBlack(x, y))
                    totalCorrect += curr;
            }
        }

        return totalCorrect / total;
    }

    @Override
    public void train(BWPixelMatrix... mats) {
        float[][] myMatrix = getMatrix();
        float[][] newMat = new float[getWidth()][getHeight()];

        for (BWPixelMatrix mat : mats) {
            checkDimensions(mat.getMatrix());

            float[][] matrix = mat.getMatrix();

            for (int x = 0; x < getWidth(); x++) {
                for (int y = 0; y < getHeight(); y++) {
                    newMat[x][y] += matrix[x][y];
                }
            }
        }

        for (int x = 0; x < getWidth(); x++) {
            for (int y = 0; y < getHeight(); y++) {
                myMatrix[x][y] = newMat[x][y] / mats.length;
            }
        }
    }
}
