package org.virus.ocr.core.mat.pixel;

import org.virus.ocr.core.mat.Matrix;

/**
 * Created with IntelliJ IDEA.
 * User: Muhatashim
 * Date: 12/31/13
 * Time: 2:01 PM
 */
public interface OCR<T extends Matrix> {

    /**
     * Finds a match difference percentage with <code>mat</code>.
     */
    float match(T mat);

    /**
     * Trains this matrix with <code>mat</code> using CMAP.
     * <p/>
     * DO NOT recall this method after training unless if you want to override previous knowledge.
     */
    void train(T... mats);
}
