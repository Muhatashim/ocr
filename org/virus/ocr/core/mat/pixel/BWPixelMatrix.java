package org.virus.ocr.core.mat.pixel;

import org.virus.ocr.core.mat.Matrix;

import java.awt.image.BufferedImage;

/**
 * Created with IntelliJ IDEA.
 * User: Muhatashim
 * Date: 12/31/13
 * Time: 2:56 PM
 */
public class BWPixelMatrix extends Matrix {

    public static final int BLACK = 1;
    /**
     * Will always will be less than <code>BLACK</code>
     */
    public static final int WHITE = 0;

    /**
     * Creates an array with the specified initial array.
     * Each element is either a 1 or a 0, 1 meaning the pixel is black or 0 for white.
     * <p/>
     * Precondition: array.length > 0
     * <p/>
     *
     * @param array [x][y]
     * @see org.virus.ocr.core.mat.pixel.BWPixelMatrix#generateBWMap(java.awt.image.BufferedImage)
     */
    public BWPixelMatrix(float[][] array) {
        super(array);
    }

    public boolean isBlack(int x, int y) {
        return getMatrix()[x][y] == BLACK;
    }

    int numBlacks() {
        int blacks = 0;

        for (int x = 0; x < getWidth(); x++) {
            for (int y = 0; y < getHeight(); y++) {
                if (isBlack(x, y))
                    blacks++;
            }
        }

        return blacks;
    }

    public static float[][] generateBWMap(BufferedImage image) {
        float[][] pixels = new float[image.getWidth()][image.getHeight()];

        for (int x = 0; x < pixels.length; x++) {
            for (int y = 0; y < pixels[0].length; y++) {
                if (image.getRGB(x, y) < 0xffffffff)
                    pixels[x][y] = BLACK;
            }
        }

        return pixels;
    }
}
