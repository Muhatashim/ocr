package org.virus.ocr.core.mat;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: Muhatashim
 * Date: 12/31/13
 * Time: 12:55 PM
 */
public class Matrix implements Serializable {

    private final int width;
    private final int height;

    private final float[][] matrix;

    /**
     * Creates an array with the specified initial array.
     * <p/>
     * Precondition: array.length > 0
     *
     * @param array [x][y]
     */
    public Matrix(float[][] array) {
        matrix = array;

        width = array.length;
        height = array[0].length;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public float[][] getMatrix() {
        return matrix;
    }

    protected void checkDimensions(float[][] diffMatrix) {
        if (matrix.length != diffMatrix.length || matrix[0].length != diffMatrix[0].length)
            throw new Error("Dimensions not same");
    }

    public float total() {
        float total = 0;

        for (float[] arr : matrix)
            for (float val : arr)
                total += val;

        return total;
    }
}
