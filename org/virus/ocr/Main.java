package org.virus.ocr;

import org.virus.ocr.core.mat.pixel.BWMatrix;
import org.virus.ocr.core.mat.pixel.BWPixelMatrix;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Muhatashim
 * Date: 12/31/13
 * Time: 12:54 PM
 */
public class Main {

    public static void main(String[] args) throws IOException {
        File file = new File("C:\\Users\\Muhatashim\\Desktop\\Trainer");
        File[] files = file.listFiles();

        BWMatrix mat = new BWMatrix(new float[35][33]);
        List<BWPixelMatrix> a = new ArrayList<>();

        for (File curr : files) {
            if (curr.getName().startsWith("A")) {
                BWPixelMatrix bwpm = new BWPixelMatrix(BWPixelMatrix.generateBWMap(ImageIO.read(curr)));
                a.add(bwpm);
            }
        }
        mat.train(a.toArray(new BWPixelMatrix[a.size()]));

        float match = mat.match(new BWPixelMatrix(BWPixelMatrix.generateBWMap(
                ImageIO.read(new File("C:\\Users\\Muhatashim\\Desktop\\Trainer\\test.png")))));
        System.out.println(match);
    }
}
